#!/bin/bash
cd "$(dirname "$0")"

: "${DESTDIR:=/}"
: "${IMAGE_PREFIX:=audio-plugin-builder}"
PLUGIN="$1"
BASE_IMAGE="$IMAGE_PREFIX-base"
PLUGIN_IMAGE="$IMAGE_PREFIX-$PLUGIN"

# validate plugin arg
[[ -z "$PLUGIN" ]] && echo "Usage: $0 <plugin_name>" && exit 1
! [[ -a "$PLUGIN/Dockerfile" ]] && echo "Invalid plugin name" && exit 1

docker build base -t "$BASE_IMAGE"
docker build "$PLUGIN" --build-arg "BASE_IMAGE=$BASE_IMAGE" -t "$PLUGIN_IMAGE"

docker save "$PLUGIN_IMAGE" | tar -x -O --wildcards '*/layer.tar' | tar -xv -C "$DESTDIR"
